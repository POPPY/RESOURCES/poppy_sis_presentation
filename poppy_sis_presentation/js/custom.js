var scale = Number($('.img-zoom').attr('data-scale'));
var lastScrollTop = 0;

$('.img-zoom')
// img-zoom mouse actions
    .on('mouseover', function () {
        $(this).children('.photo').css({'transform': 'scale(' + scale + ')'});
    })
    .on('mouseout', function () {
        $(this).children('.photo').css({'transform': 'scale(1)'});
    })
    .on('mousemove', function (e) {
        if ($(this).hasClass('commands')) {
            $(this).children('.photo').css({'transform': 'translate(0, ' + '-' + ((e.pageY - $(this).offset().top) / $(this).height()) * 160 + '%'});

        }
        else {
            $(this).children('.photo').css({'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 + '%'});

        }
    })
    // img-zooms set up
    .each(function () {
        $(this)
        // add a photo container
            .append('<div class="photo"></div>')
            // some text just to show zoom level on current item in this example
            // set up a background image for each img-zoom based on data-image attribute
            .children('.photo').css({'background-image': 'url(' + $(this).attr('data-image') + ')'});
    })
    .bind('mousewheel DOMMouseScroll', function (event) {
        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
            scale = scale + 0.3;
            $(this).mouseover();
        }
        else {

            scale = scale - 0.3;
            if (scale < 1) {
                scale = 1
            }
            $(this).mouseover();

        }
    });
